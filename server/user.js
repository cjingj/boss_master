const express = require("express");
const utils = require("utility");

const Router = express.Router();
const model = require("./model");
const User = model.getModel("user");
const Chat = model.getModel("chat");
const _filter = { pwd: 0, __v: 0 };
// 删除某个集合
// Chat.remove({}, (e, d) => {});  
Router.get("/list", function (req, res) {
  const { type } = req.query;
  //只返回一个
  User.find({ type }, function (err, doc) {
    return res.json({ code: 0, data: doc });
  });
});

Router.post("/update", function (req, res) {
  const userid = req.cookies.userid;
  if (!userid) {
    return json.dumps({ code: 1 });
  }
  const body = req.body;
  //找到userid为userid的一个,用body替换
  User.findByIdAndUpdate(userid, body, function (err, doc) {
    const data = Object.assign(
      {},
      {
        user: doc.user,
        type: doc.type,
      },
      body
    );
    return res.json({ code: 0, data });
  });
});
Router.post("/login", function (req, res) {
  const { user, pwd } = req.body;
  //找到是否存在  条件是user和pwd同时满足
  User.findOne({ user, pwd: md5Pwd(pwd) }, _filter, function (err, doc) {
    if (!doc) {
      return res.json({ code: 1, msg: "用户名或者密码错误" });
    }
    res.cookie("userid", doc._id);
    return res.json({ code: 0, data: doc });
  });
});
Router.post("/register", function (req, res) {
  const { user, pwd, type } = req.body;
  User.findOne({ user }, function (err, doc) {
    if (doc) {
      return res.json({ code: 1, msg: "用户名重复" });
    }
	//创建新的一条数据的写法
    const userModel = new User({ user, type, pwd: md5Pwd(pwd) });
    userModel.save(function (e, d) {
      if (e) {
        return res.json({ code: 1, msg: "后端出错了" });
      }
      const { user, type, _id } = d;
      res.cookie("userid", _id);
      return res.json({ code: 0, data: { user, type, _id } });
    });
  });
});
Router.get("/info", function (req, res) {
  const { userid } = req.cookies;
  if (!userid) {
    return res.json({ code: 1 });
  }
  //查找
  User.findOne({ _id: userid }, _filter, function (err, doc) {
    if (err) {
      return res.json({ code: 1, msg: "后端出错了" });
    }
    if (doc) {
      return res.json({ code: 0, data: doc });
    }
  });
  // 用户有没有cookie
});
//更新信息
Router.post("/update", function (req, res) {
  const userid = req.cookies.userid;
  if (!userid) {
    return json.dumps({ code: 1 });
  }
  const body = req.body;
  //找到userid 用body替换
  User.findByIdAndUpdate(userid, body, function (err, doc) {
    const data = Object.assign(
      {},
      {
        user: doc.user,
        type: doc.type,
      },
      body
    );
    return res.json({ code: 0, data });
  });
});
//获取聊天信息列表
Router.get("/getmsglist", function (req, res) {
  const user = req.cookies.userid;
//把所有的人查出来 维护一份 _id => {name,avatar} 的表
  User.find({}, function (e, userdoc) {
    let users = {};
    userdoc.forEach((v) => {
      users[v._id] = { name: v.user, avatar: v.avatar };
    });
	//在这里过滤
    Chat.find({ $or: [{ from: user }, { to: user }] }, function (err, doc) {
      if (err) {
        return res.json({ code: 1, msg: "后端出错了" });
      }
      if (doc) {
        return res.json({ code: 0, msgs: doc, users: users });
      }
    });
  });
});
//标识已读
Router.post("/readmsg", function (req, res) {
	//userid为当前用户,from为target用户，因为从未读到已读肯定是
	// from为target, to为 userid
  const userid = req.cookies.userid;
  const { from } = req.body;
  Chat.update(
    { from, to: userid },
    { $set: { read: true } },
    { multi: true },
    function (err, doc) {
      if (!err) {
		  //doc.nModified为修改的数量
        return res.json({ code: 0, num: doc.nModified });
      }
      return res.json({ code: 1, msg: "修改失败" });
    }
  );
});
function md5Pwd(pwd) {
  const salt = "imooc_is_good_3957x8yza6!@#IUHJh~~";
  return utils.md5(utils.md5(pwd + salt));
}

module.exports = Router;
