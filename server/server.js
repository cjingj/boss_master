const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const model = require("./model");
const Chat = model.getModel("chat");
const userRouter = require("./user");
const app = express();
const server = require("http").createServer(app);
// const server = require("http").Server(app);
const io = require("socket.io")(server, { cors: true });


app.use(cookieParser());
app.use(bodyParser.json());
app.use("/user", userRouter);
//记住这里要把 app 改成server
server.listen(9093, function () {
  console.log("Node app start at port 9093");
});
io.on('connection',(socket) =>{
	console.log('有人连接啦')
	socket.on('sendmsg',(data)=>{
		const {from,to,msg} = data
		const chatid = [from, to].sort().join('_')
		Chat.create({chatid, from, to, content:msg}, function(err, doc){ //数据库存入数据
			//向前端出发recvmsg事件
			io.emit('recvmsg', Object.assign({}, doc._doc))
	   })
	})
})
