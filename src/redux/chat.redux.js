import axios from "axios";
import io from "socket.io-client";
const socket = io("ws://localhost:9093");
//action type
const MSG_LIST = "MSG_LIST"; //获取聊天列表
const MSG_RECV = "MST_RECV"; //读取信息
const MSG_READ = "MSG_READ"; //标识已读
const initState = {
    chatmsg: [],
    users: {},
    unread: 0  //实时维护未读消息的数量
}
export function chat(state = initState, action) {
    switch(action.type){
        case MSG_LIST:
            console.log('msglist',action)
             return {...state, users:action.payload.users, chatmsg:action.payload.msgs, unread:action.payload.msgs.filter(v=>!v.read&&v.to==action.payload.userid).length} //未读而且是发给我的才需要记录未读的数量

        case MSG_RECV:
            console.log('执行次数')
            //同样 是发给我的数量才加一  我自己发出去的不纳入
             const n = action.payload.to==action.userid ? 1 : 0
             return {...state, chatmsg:[...state.chatmsg, action.payload], unread:state.unread+n}
        case MSG_READ:
             const {from, num} = action.payload
             return {...state, chatmsg:state.chatmsg.map(v=>({...v, read:from==v.from?true:v.read})) , unread:state.unread-num}
        default:
             return state
    }
}
//获取所有用户的map,  获取以下条件 $or: [{ from: user }, { to: user }] 的 聊天信息
function msgList(msgs, users, userid) {
  return {
    type: MSG_LIST,
    payload: { msgs, users, userid },
  };
}
//每次发送信息或者接受信息都会执行的操作
function msgRecv(msg, userid){
    return {userid, type:MSG_RECV, payload:msg}
}
//退出聊天窗口的时候执行的操作，标记未读为已读
function msgRead({ from, to, num }) {
  return {
    type: MSG_READ,
    payload: { from, to, num },
  };
}
//操作数据的方法

//读取某个人的聊天信息
export function readMsg(from){
    //传进来的from是to来的，因为要将from为对面传过来的id，to为userid的所以read设置为true
    return (dispatch, getState)=>{
        //更新已读
        axios.post('/user/readmsg',{from})
            .then(res=>{
                const userid = getState().user._id
                if(res.status==200&&res.data.code==0){
                    //num为标记为read= true的数量
                    dispatch(msgRead({userid, from, num:res.data.num}))
                }
            })
    }
}
//接受服务端发送的信息
export function recvMsg(){
    return (dispatch, getState)=>{
        socket.on('recvmsg', function(data){
            // console.log('recvmsg执行次数', data)
            const userid = getState().user._id
            dispatch(msgRecv(data, userid))
        })
    }
}
export function sendMsg({from, to, msg}){
    return dispatch=>{
        socket.emit('sendmsg', {from, to, msg})
    }
}
//没有传个人id的原因是服务端获取了cookie的个人id
export function getMsgList(){  
    return (dispatch, getState)=>{   //redux中使用其它地方的数据，通过getState获取全部的状态
        axios.get('/user/getmsglist')
           .then(res=>{
               if(res.status==200 && res.data.code==0){
                   //console.log('getState',getState())
                   const userid = getState().user._id
                   dispatch(msgList(res.data.msgs, res.data.users, userid))
               }
           })
    }
}