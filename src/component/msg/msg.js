import React from "react";
import { List, Badge } from "antd-mobile";
import { connect } from "react-redux";

@connect((state) => state)
class Msg extends React.Component {
  getLast = (arr) => {
    return arr[arr.length - 1];
  };
  render() {
    const Item = List.Item;
    const Brief = Item.Brief;
    const userid = this.props.user._id; //当前用户id
    const userinfo = this.props.chat.users; //全部用户的_id => {}
    const msgGroup = {};
    //charid是和每个人聊天的唯一值
    this.props.chat.chatmsg.forEach((v) => {
      msgGroup[v.chatid] = msgGroup[v.chatid] || [];
      msgGroup[v.chatid].push(v);
    });
    //用户信息数组的数组、排序,因为数组的最后一项为最新聊天，所以用他们排序
    const chatList = Object.values(msgGroup).sort((a, b) => {
      const a_last = this.getLast(a).create_time;
      const b_last = this.getLast(b).create_time;
      return b_last - a_last;
    });
    return (
      <div>
        {chatList.map((v) => {
          //获取最后一条信息，放在外面显示
          const lastItem = this.getLast(v);
          //拿到对方的id
          const targetId = v[0].from == userid ? v[0].to : v[0].from;
          //!read而且to 为 自己才计数,因为自己发出去的不算入未读
          const unreadNum = v.filter((v) => !v.read && v.to == userid).length;
          if (!userinfo[targetId]) {
            return null;
          }
          return (
            <List key={lastItem._id}>
              <Item
                extra={<Badge text={unreadNum}></Badge>}
                thumb={require(`../img/${userinfo[targetId].avatar}.png`)}
                arrow="horizontal"
                onClick={() => {
                  this.props.history.push(`/chat/${targetId}`);
                }}
              >
                {lastItem.content}
                <Brief>{userinfo[targetId].name}</Brief>
              </Item>
            </List>
          );
        })}
      </div>
    );
  }
}
export default Msg;
